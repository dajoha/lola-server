use std::fmt;

use diesel::result;

use crate::{misc::CowStr, session::SessionId};

pub type ApiResult<T = ()> = Result<T, ApiError>;

#[derive(Debug)]
pub enum ApiError {
    Custom(CowStr),
    SessionNotFound(SessionId),
    AdminUser(AdminUserError),
    /// This error is treated differently compared to all the others: their precise description
    /// will never be sent to the end-user, and they will have a 500 status code:
    Internal(InternalError),
}

impl std::error::Error for ApiError {}

impl ApiError {
    pub fn custom(message: impl Into<CowStr>) -> Self {
        Self::Custom(message.into())
    }

    pub fn custom_internal(message: impl Into<CowStr>) -> Self {
        Self::Internal(InternalError::Custom(message.into()))
    }

    pub fn into_internal_string(self) -> CowStr {
        match self {
            Self::Internal(err) => err.into(),
            err => err.into(),
        }
    }
}

impl From<&ApiError> for CowStr {
    fn from(err: &ApiError) -> CowStr {
        match err {
            ApiError::Custom(err) => err.clone(),
            ApiError::SessionNotFound(id) => format!("Session not found: '{}'", id).into(),
            ApiError::AdminUser(err) => err.into(),
            ApiError::Internal(_) => "Internal server error".into(),
        }
    }
}

impl From<ApiError> for CowStr {
    fn from(err: ApiError) -> CowStr {
        (&err).into()
    }
}

impl From<result::Error> for ApiError {
    fn from(err: result::Error) -> ApiError {
        Self::Internal(InternalError::Query(err))
    }
}

impl From<String> for ApiError {
    fn from(message: String) -> ApiError {
        Self::Custom(CowStr::Owned(message))
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", CowStr::from(self))
    }
}

pub enum AdminUserError {
    NoAuthHeader,
    InvalidAuthHeader,
    Jwt(jsonwebtoken::errors::Error),
}

impl From<&AdminUserError> for CowStr {
    fn from(err: &AdminUserError) -> CowStr {
        match err {
            AdminUserError::NoAuthHeader => "No authentication header".into(),
            AdminUserError::InvalidAuthHeader => "Invalid authentication header".into(),
            AdminUserError::Jwt(e) => format!("{}", e).into(),
        }
    }
}

impl From<AdminUserError> for CowStr {
    fn from(err: AdminUserError) -> CowStr {
        (&err).into()
    }
}

impl From<AdminUserError> for ApiError {
    fn from(err: AdminUserError) -> ApiError {
        ApiError::AdminUser(err)
    }
}

impl fmt::Display for AdminUserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", CowStr::from(self))
    }
}

impl fmt::Debug for AdminUserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

#[derive(Debug, PartialEq)]
pub enum InternalError {
    Query(result::Error),
    Custom(CowStr),
}

impl From<InternalError> for CowStr {
    fn from(err: InternalError) -> CowStr {
        match err {
            InternalError::Query(err) => CowStr::Owned(err.to_string()),
            InternalError::Custom(err) => err,
        }
    }
}
