use rocket::{http::Status, response::{self, Responder}, serde::json::Json};
use serde::Serialize;

use crate::error::{ApiError, ApiResult};

#[derive(Serialize)]
pub struct ApiResponse<P = ()>
where
    P: Serialize
{
    success: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    payload: Option<P>,
    #[serde(skip_serializing_if = "Option::is_none")]
    error: Option<String>,
    #[serde(skip)]
    status: Status,
}

impl<P: Serialize> ApiResponse<P> {
    pub fn success(payload: P) -> ApiResponse<P> {
        Self {
            success: true,
            error: None,
            payload: Some(payload),
            status: Status::Ok,
        }
    }

    pub fn success_no_payload() -> ApiResponse<P> {
        Self {
            success: true,
            error: None,
            payload: None,
            status: Status::Ok,
        }
    }

    pub fn error(message: impl Into<String>) -> ApiResponse<P> {
        Self {
            success: false,
            error: Some(message.into()),
            payload: None,
            status: Status::Ok,
        }
    }

    pub fn internal_error(message: impl Into<String>) -> ApiResponse<P> {
        Self {
            success: false,
            error: Some(message.into()),
            payload: None,
            status: Status::InternalServerError,
        }
    }
}

impl<'r, P: Serialize> Responder<'r, 'static> for ApiResponse<P> {
    fn respond_to(self, request: &'r rocket::Request<'_>) -> response::Result<'static> {
        let status = self.status;
        let mut response = Json(self).respond_to(request)?;
        response.set_status(status);

        Ok(response)
    }
}

impl<P: Serialize> From<ApiResult<P>> for ApiResponse<P> {
    fn from(result: ApiResult<P>) -> ApiResponse<P> {
        match result {
            Ok(payload) => Self::success(payload),
            Err(err) => err.into(),
        }
    }
}

impl<P: Serialize> From<ApiError> for ApiResponse<P> {
    fn from(err: ApiError) -> ApiResponse<P> {
        let message = err.to_string();
        match err {
            ApiError::Internal(_internal_message) => {
                #[cfg(debug_assertions)]
                println!("   >> INTERNAL ERROR: {:?}", _internal_message);

                Self::internal_error(message)
            }
            _ => Self::error(message),
        }
    }
}
