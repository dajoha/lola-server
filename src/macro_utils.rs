macro_rules! lock {
    ($name:expr, $alias:ident, $block:block) => {{
        #[allow(unused_mut)]
        let mut $alias = $name.lock().unwrap();
        $block
    }};
    ($name:ident, $block:block) => {{
        #[allow(unused_mut)]
        let mut $name = $name.lock().unwrap();
        $block
    }};
}

macro_rules! send_message {
    ($app:ident, $tx:ident, $message:expr) => {{
        use std::sync::mpsc::channel;
        let ($tx, rx) = channel();
        lock!($app, {
            $app.send_message($message);
        });
        rx.recv().unwrap()
    }};
}
