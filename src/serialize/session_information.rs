use chrono::NaiveDateTime;
use serde::Serialize;

use crate::session::{SessionId, SessionCommand};

/// Used to send public information about a session.
#[derive(Clone, Serialize)]
pub struct SessionInformation {
    pub session_id: SessionId,
    pub is_public: bool,
    pub commands: Vec<SessionCommand>,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}
