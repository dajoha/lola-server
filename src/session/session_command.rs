use serde::Serialize;

use lola::context::ContextRef;
use lola::parse::{parse_expr, display_error};
use lola::value::Format;

use crate::serialize::{SerializeResult, SerializeId};

const UNDERSCORE: &str = "_";

/// A command result which will be saved into the session.
#[derive(Debug, Clone, Serialize)]
pub struct SessionCommand {
    pub command_id: SerializeId,
    pub input: String,
    pub output: String,
    pub result: SerializeResult<String, String>,
}

impl SessionCommand {
    pub fn new(command_id: u64, input: String, output: String, result: &Result<String, String>) -> Self {
        Self {
            command_id: SerializeId::from(command_id),
            input,
            output,
            result: SerializeResult::from(result.clone()),
        }
    }

    pub fn command_id(&self) -> u64 {
        self.command_id.0
    }

    pub fn input(&self) -> &str {
        self.input.as_str()
    }

    /// Creates a new session command by evaluating the given input code.
    pub fn from_eval(command_id: u64, code: String, cxt: &mut ContextRef) -> Self {
        let result = match parse_expr(&code, cxt.container, None) {
            Ok(expr) => {
                match expr.eval(cxt) {
                    Ok(value) => {
                        cxt.scope.get_var_mut(
                            UNDERSCORE,
                            |underscore| *underscore = value.clone()
                        ).unwrap();
                        if value.is_void() {
                            Ok("".to_string())
                        } else {
                            Ok(value.format(cxt.container.format_config(), true))
                        }
                    },
                    Err(err) => {
                        Err(format!("{}", err))
                    },
                }
            },
            Err(err) => {
                Err(display_error(&err))
            },
        };

        // Retrieve the command output:
        let output = cxt.container.get_buffer().unwrap();
        cxt.container.empty_buffer();

        SessionCommand::new(command_id, code, output, &result)
    }
}
