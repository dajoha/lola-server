use std::collections::{BTreeMap, btree_map::Entry};
use std::sync::mpsc::{Sender, Receiver};

use serde::Serialize;
use rand::{thread_rng, Rng};
use diesel::prelude::*;

use crate::error::{ApiError, ApiResult};
use crate::session::{
    Session,
    SessionId,
    LolaSession,
    SendableLolaSession,
    SessionCommand,
    SendableSession,
};
use crate::serialize::{SessionInformation, SessionSummary};
use crate::misc::OneOrMany::Many;
use crate::db::{
    models::NewSession,
    db_limits::DbLimits,
    lola_query,
};
use crate::has_database;

#[derive(Debug)]
pub enum SessionManagerMessage {
    // Create session:
    CreateSession(Sender<ApiResult<SessionId>>),
    CreateNamedSession(SessionId, Sender<ApiResult>),
    CloneSession(SessionId, Sender<ApiResult<SessionId>>),

    // Modify a session:
    RenameSession(SessionId, SessionId, Sender<ApiResult>),
    DeleteSession(SessionId, Sender<ApiResult>),
    ResetSession(SessionId, Sender<ApiResult>),
    SetSessionAccessibility(SessionId, bool, Sender<ApiResult>),

    // Change session state (internal messages, not publicly accessible):
    FreeSession(SessionId, SendableLolaSession, Vec<SessionCommand>),
    ReplaceSession(SendableSession),

    // Query session information:
    PingSession(SessionId, Sender<ApiResult>),
    GetSessionInformation(SessionId, Sender<ApiResult<SessionInformation>>),
    ListPublicSessionsSummaries(Sender<Vec<SessionSummary>>),
    GetGlobalStats(Sender<ApiResult<SessionManagerStats>>),

    // Run code, modify commands:
    RunCode(SessionId, Vec<String>, Sender<ApiResult<Vec<SessionCommand>>>),
    DeleteCommand(SessionId, u64, Sender<ApiResult<SessionInformation>>),
    EditCommand(SessionId, u64, String, Sender<ApiResult<SessionInformation>>),
    InsertCommands(SessionId, u64, Vec<String>, Sender<ApiResult<SessionInformation>>),
}

/// The owner of all lola sessions.
///
/// When a session is running, then the ownership of the lola session is temporarily passed to the
/// runner thread, then it is given back to the session manager when the
/// `SessionManagerMessage::FreeSession` message is sent by the runner thread.
pub struct SessionManager {
    sessions: BTreeMap<SessionId, Session>,
    tx: Sender<SessionManagerMessage>,
    rx: Option<Receiver<SessionManagerMessage>>,
}

impl SessionManager {
    /// Creates a new new session manager. Typically, happens only once for a given rocket server.
    pub fn new(tx: Sender<SessionManagerMessage>, rx: Receiver<SessionManagerMessage>) -> Self {
        Self {
            sessions: BTreeMap::new(),
            tx,
            rx: Some(rx),
        }
    }

    pub fn handle_messages(mut self) {
        let rx = self.rx.take().unwrap();
        for message in rx {
            match message {
                SessionManagerMessage::CreateSession(sender_tx) => {
                    let result = self.create_session();
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::CreateNamedSession(session_id, sender_tx) => {
                    let result = self.create_named_session(session_id);
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::RenameSession(session_id, new_session_id, sender_tx) => {
                    let result = self.rename_session(&session_id, new_session_id);
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::DeleteSession(session_id, sender_tx) => {
                    let result = self.delete_session(&session_id);
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::ResetSession(session_id, sender_tx) => {
                    let result = self.reset_session(session_id);
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::CloneSession(session_id, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.clone_session(session_id, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::FreeSession(session_id, sendable_lola_session, old_commands) => {
                    let lola_session = sendable_lola_session.unpack();
                    // TODO: remove `ok()`
                    self.free_session(&session_id, lola_session, old_commands).ok();
                },
                SessionManagerMessage::ReplaceSession(new_session) => {
                    self.replace_session(new_session);
                },
                SessionManagerMessage::SetSessionAccessibility(session_id, is_public, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.set_session_accessibility(session_id, is_public, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::PingSession(session_id, sender_tx) => {
                    let result = self.ping_session(&session_id);
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::GetSessionInformation(session_id, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.get_session_information(session_id, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::ListPublicSessionsSummaries(sender_tx) => {
                    let result = self.list_public_sessions_summaries();
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::GetGlobalStats(sender_tx) => {
                    let result = self.get_global_stats();
                    sender_tx.send(result).unwrap();
                },
                SessionManagerMessage::RunCode(session_id, code, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.run_code(session_id, code, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::DeleteCommand(session_id, command_id, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.delete_command(session_id, command_id, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::EditCommand(session_id, command_id, new_input, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.edit_command(session_id, command_id, new_input, sender_tx, session_manager_tx);
                },
                SessionManagerMessage::InsertCommands(session_id, before_command_id, code, sender_tx) => {
                    let session_manager_tx = self.tx.clone();
                    self.insert_commands(session_id, before_command_id, code, sender_tx, session_manager_tx);
                },
            }
        }
    }

    /// Checks if the maximum number of sessions has not been reached.
    /// TODO: this check method implies an extra db connection lock
    pub fn check_can_create_session(&self, session_id: &SessionId) -> ApiResult {
        lock_db_connection!(connection, {
            connection.can_add_session(session_id)?;
        });
        Ok(())
    }

    /// Checks if the name of a session is not too long.
    /// TODO: this check method implies an extra db connection lock
    pub fn check_can_rename_session(&self, old_id: &SessionId, new_id: &SessionId) -> ApiResult {
        lock_db_connection!(connection, {
            connection.can_set_session_name(Some(old_id), new_id)?;
        });
        Ok(())
    }

    /// Creates a new session.
    ///
    /// Returns the new session id.
    pub fn create_session(&mut self) -> ApiResult<SessionId> {
        let session_id = self.new_session_id();
        self.check_can_create_session(&session_id)?;
        let session = Session::new(session_id.clone(), None);

        if has_database!() {
            use crate::db::schema::sessions::dsl::*;

            lock_db_connection!(connection, {
                diesel::insert_into(sessions)
                    .values(&NewSession::from(&session))
                    .execute(connection)
                    .expect("Error saving new session");
            });
        }

        self.sessions.insert(session_id.clone(), session);

        Ok(session_id)
    }

    /// Creates a new named session.
    pub fn create_named_session(&mut self, session_id: SessionId) -> ApiResult {
        self.check_can_create_session(&session_id)?;

        let session = Session::new(session_id.clone(), None);

        if has_database!() {
            use crate::db::schema::sessions::dsl::*;

            lock_db_connection!(connection, {
                diesel::insert_into(sessions)
                    .values(&NewSession::from(&session))
                    .execute(connection)
                    .expect("Error saving new session");
            });
        }

        self.sessions.insert(session_id, session);
        Ok(())
    }

    /// Clones the given session.
    pub fn clone_session(
        &mut self,
        session_id: SessionId,
        tx: Sender<ApiResult<SessionId>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        let new_session_id = self.new_session_id();
        if let Some(session) = self.sessions.get(&session_id) {
            session.clone_session(new_session_id, tx, session_manager_tx);
        } else {
            Session::wakeup_and_clone_session(session_id, new_session_id, tx, session_manager_tx);
        }
    }

    /// Renames a session.
    pub fn rename_session(&mut self, session_id: &SessionId, new_session_id: SessionId)
        -> ApiResult
    {
        self.check_can_rename_session(session_id, &new_session_id)?;

        let mut session = match self.sessions.remove(session_id) {
            Some(session) => Ok(session),
            None => Err(ApiError::SessionNotFound(session_id.clone())),
        }?;
        session.set_id(new_session_id.clone());
        session.update_timestamp();

        if has_database!() {
            use crate::db::schema::sessions::dsl::*;

            lock_db_connection!(connection, {
                diesel::update(sessions.filter(id.eq(session_id.inner_str())))
                    .set((
                        id.eq(new_session_id.inner_str()),
                        updated_at.eq(session.updated_at),
                    ))
                    .execute(connection)
                    .ok();
            });
        }

        self.sessions.insert(new_session_id, session);

        Ok(())
    }

    /// Deletes the given session definitively.
    pub fn delete_session(&mut self, session_id: &SessionId) -> ApiResult {
        self.sessions.remove(session_id);

        if has_database!() {
            lock_db_connection!(connection, {
                lola_query::delete_session(connection, session_id)?;
            });
        }

        Ok(())
    }

    /// Retrieves public information about the given session.
    pub fn get_session_information(
        &mut self,
        session_id: SessionId,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        send_err_to_tx!(tx, {
            match self.sessions.entry(session_id.clone()) {
                Entry::Occupied(entry) => {
                    let session = entry.get();
                    tx.send(Ok(session.get_information())).unwrap();
                }
                Entry::Vacant(_) => {
                    Session::wakeup_session(session_id, tx, session_manager_tx);
                }
            }
        });
    }

    /// Resets the given session.
    pub fn reset_session(&mut self, session_id: SessionId) -> ApiResult {
        if has_database!() {
            lock_db_connection!(connection, {
                let fetch_session = lola_query::get_session(connection, &session_id)?;
                let session = Session::new(session_id.clone(), Some(fetch_session.created_at));
                lola_query::reset_session(connection, &session_id, session.updated_at)?;
                self.sessions.insert(session_id, session);
            });
        } else {
            panic!("TODO")
        }

        Ok(())
    }

    /// Checks if the given session exists.
    pub fn ping_session(&mut self, session_id: &SessionId) -> ApiResult {
        if self.has_session(session_id)? {
            Ok(())
        } else {
            Err(ApiError::SessionNotFound(session_id.to_owned()))
        }
    }

    /// Deletes the given command of the given session.
    pub fn delete_command(
        &mut self,
        session_id: SessionId,
        command_id: u64,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        self.find_session_mut_then(
            session_id,
            tx.clone(),
            session_manager_tx.clone(),
            move |session| session.delete_command(command_id, tx, session_manager_tx),
        );
    }

    /// Edits the given command of the given session.
    pub fn edit_command(
        &mut self,
        session_id: SessionId,
        command_id: u64,
        new_input: String,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        self.find_session_mut_then(
            session_id,
            tx.clone(),
            session_manager_tx.clone(),
            move |session| session.edit_command(command_id, new_input, tx, session_manager_tx),
        );
    }

    /// Insert a new command before the given command of the given session.
    pub fn insert_commands(
        &mut self,
        session_id: SessionId,
        before_command_id: u64,
        code: Vec<String>,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        self.find_session_mut_then(
            session_id,
            tx.clone(),
            session_manager_tx.clone(),
            move |session| session.insert_commands(before_command_id, code, tx, session_manager_tx),
        );
    }

    /// Runs the given code within the given session.
    ///
    /// The result of the process is sent to the `tx` transmitter.
    ///
    /// The function needs the session manager transmitter `session_manager_tx` in order to send
    /// the `SessionManagerMessage::FreeSession` message, which will inform the session manager
    /// that the given session can be flagged as `SessionState::Sleeping(_)`, once the code has
    /// been processed.
    pub fn run_code(
        &mut self,
        session_id: SessionId,
        code: Vec<String>,
        tx: Sender<ApiResult<Vec<SessionCommand>>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        self.find_session_mut_then(
            session_id,
            tx.clone(),
            session_manager_tx.clone(),
            move |session| session.run_code(code, tx, session_manager_tx),
        );
    }

    /// Makes the given session available for new code runs.
    ///
    /// The ownership of the lola session is given back to the session manager.
    pub fn free_session(
        &mut self,
        session_id: &SessionId,
        lola_session: LolaSession,
        old_commands: Vec<SessionCommand>,
    ) -> ApiResult {
        match self.sessions.get_mut(session_id) {
            Some(session) => {
                session.free(lola_session);
                session.push_command(Many(old_commands));
                session.update_timestamp();

                if has_database!() {
                    session.update_db_commands()?;
                }

                Ok(())
            },
            None => {
                panic!("free_session(): Session not found: {}", session_id);
            },
        }
    }

    /// Replaces the given session.
    pub fn replace_session(&mut self, session: SendableSession) {
        let session = session.unpack();
        self.sessions.insert(session.id().clone(), session);
    }

    /// Defines the accessibility of a session.
    pub fn set_session_accessibility(
        &mut self,
        session_id: SessionId,
        is_public: bool,
        tx: Sender<ApiResult>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        self.find_session_mut_then(
            session_id.clone(),
            tx.clone(),
            session_manager_tx,
            move |session| {
                session.set_public(is_public);
                session.update_timestamp();

                if has_database!() {
                    send_err_to_tx!(tx, {
                        lock_db_connection!(connection, {
                            lola_query::set_session_accessibility(
                                connection,
                                &session_id,
                                session.updated_at,
                                is_public,
                            )?;
                        });
                    });
                }

                tx.send(Ok(())).unwrap();
            }
        );
    }

    /// Gets an iterator over all the public sessions.
    pub fn iter_public_sessions(&self) -> impl Iterator<Item = &Session> {
        self.sessions.values()
            .filter(|session| session.is_public())
    }

    /// Gets a list of all public sessions summaries.
    pub fn list_public_sessions_summaries(&self) -> Vec<SessionSummary> {
        self.iter_public_sessions()
            .map(|session| session.get_summary())
            .collect::<Vec<_>>()
    }

    /// Retrieves global statistics about the server.
    pub fn get_global_stats(&self) -> ApiResult<SessionManagerStats> {
        let awake_sessions = SessionManagerSessionStats {
            nb_sessions: self.sessions.len() as i64,
            nb_empty_sessions: self.sessions
                .values()
                .filter(|session| session.nb_commands() == 0)
                .count() as i64,
            nb_commands: self.sessions
                .values()
                .map(|session| session.nb_commands())
                .sum::<usize>() as i64,
        };

        let all_sessions = if has_database!() {
            lock_db_connection!(connection, {
                SessionManagerSessionStats {
                    nb_sessions: lola_query::nb_sessions(connection)?,
                    nb_empty_sessions: lola_query::nb_empty_sessions(connection)?,
                    nb_commands: lola_query::nb_commands(connection)?,
                }
            })
        } else {
            awake_sessions.clone()
        };

        Ok(SessionManagerStats { all_sessions, awake_sessions })
    }

    /// Checks if the given session exists.
    fn has_session(&self, session_id: &SessionId) -> ApiResult<bool> {
        let has_session = self.sessions.contains_key(session_id) || has_database!() && {
            lock_db_connection!(connection, {
                lola_query::has_session(connection, session_id)?
            })
        };

        Ok(has_session)
    }

    /// Tries to find the given session (first in the awake sessions, then in the database), then
    /// runs a closure by giving to it a mutable reference to the session.
    fn find_session_mut_then<F, S>(
        &mut self,
        session_id: SessionId,
        tx: Sender<ApiResult<S>>,
        session_manager_tx: Sender<SessionManagerMessage>,
        f: F,
    )
    where
        F: FnOnce(&mut Session) + Send + 'static,
        S: Send + 'static,
    {
        if let Some(session) = self.sessions.get_mut(&session_id) {
            f(session)
        } else {
            Session::wakeup_then_mut(session_id, tx, session_manager_tx, f);
        }
    }

    /// Creates a new, available session id.
    ///
    /// NOTE: the availability of the id is actually not checked, since the probability of
    /// collision is very weak, compared to the estimated traffic of this service.
    fn new_session_id(&self) -> SessionId {
        // TODO: check collisions
        let n = thread_rng().gen_range((u64::MAX >> 1) + 1 .. u64::MAX);
        SessionId::from(n)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize)]
pub struct SessionManagerStats {
    all_sessions: SessionManagerSessionStats,
    awake_sessions: SessionManagerSessionStats,
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize)]
pub struct SessionManagerSessionStats {
    nb_sessions: i64,
    nb_empty_sessions: i64,
    nb_commands: i64,
}
