use lola::context::{Scope, Container, ContextRef};
use lola::library::prefill_context;

use crate::session::fake_importer::FakeImporter;

pub use sendable_lola_session::*;

pub struct LolaSession {
    pub container: Container,
    pub scope: Scope,
}

impl LolaSession {
    pub fn new() -> Self {
        let mut container = Container::new(Container::buffered_writer(), FakeImporter::new());
        let mut scope = Scope::new();
        let mut cxt = ContextRef::new(&mut container, &mut scope);
        prefill_context(&mut cxt);
        Self {
            container,
            scope,
        }
    }

    pub unsafe fn into_sendable(self) -> SendableLolaSession {
        SendableLolaSession::new(self)
    }
}

impl Default for LolaSession {
    fn default() -> Self {
        Self::new()
    }
}

mod sendable_lola_session {
    use std::fmt;

    use super::LolaSession;

    pub struct SendableLolaSession(LolaSession);

    // SAFETY: cannot be created without using the unsafe method new()
    unsafe impl Send for SendableLolaSession {}

    impl SendableLolaSession {
        pub unsafe fn new(lola_session: LolaSession) -> Self {
            Self(lola_session)
        }

        pub fn unpack(self) -> LolaSession {
            self.0
        }
    }

    impl fmt::Debug for SendableLolaSession {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "(Sendable lola session)")
        }
    }
}
