use argon2::{
    Algorithm,
    Argon2,
    Params,
    PasswordHash,
    Version,
    PasswordVerifier,
    password_hash::{
        self,
        PasswordHasher,
        SaltString,
        rand_core::OsRng,
    }
};

pub fn hash_password(salt: Option<&str>, password: &[u8]) -> Result<String, password_hash::Error> {
    let salt = match salt {
        Some(salt) => SaltString::new(salt)?,
        None => gen_salt(),
    };
    let argon2 = get_argon2();
    Ok(argon2.hash_password(password, &salt)?.to_string())
}

pub fn verify_password(password: &[u8], password_hash: &str) -> Result<(), password_hash::Error> {
    let parsed_hash = PasswordHash::new(password_hash)?;
    let argon2 = get_argon2();
    argon2.verify_password(password, &parsed_hash)?;
    Ok(())
}

pub fn gen_salt() -> SaltString {
    SaltString::generate(&mut OsRng)
}

fn get_argon2() -> Argon2<'static> {
    Argon2::new(
        Algorithm::default(),
        Version::default(),
        Params::new(Params::DEFAULT_M_COST, 2, Params::DEFAULT_P_COST, None).unwrap()
    )
}
