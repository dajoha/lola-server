#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;

#[macro_use] mod macro_utils;
mod serialize;
#[macro_use]
mod db;
#[macro_use]
mod misc;
mod session;
mod routes;
mod api;
mod app;
mod error;
mod security;
mod app_config;
mod cli;

use std::{error::Error, sync::Mutex};

use app::App;
use app_config::{LolaServerConfig, lola_server_config, lola_server_figment};
use cli::{
    command,
    cli_config::{CliCommand, CliConfig},
};

async fn start_server(lola_server_config: LolaServerConfig) -> Result<(), rocket::Error> {
    println!("database feature is {}", if has_database!() { "ON" } else { "OFF" });

    let cors = rocket_cors::CorsOptions::default().to_cors().unwrap();

    rocket::custom(lola_server_figment())
        .mount("/", routes![
            routes::sessions::create::route,
            routes::sessions::rename::route,
            routes::sessions::run_code::route,
            routes::sessions::delete::route,
            routes::sessions::reset::route,
            routes::sessions::clone::route,
            routes::sessions::accessibility::route,
            routes::sessions::get::route,
            routes::sessions::ping::route,
            routes::commands::delete::route,
            routes::commands::edit::route,
            routes::commands::insert::route,
            routes::sessions::list_public::route,
            routes::auth::login::route,
            routes::version::route,
            routes::admin::stats::route,
            routes::admin::ping::route,
        ])
        .mount("/", rocket_cors::catch_all_options_routes())
        .attach(cors.clone())
        .manage(Mutex::new(App::new()))
        .manage(lola_server_config)
        .manage(cors)
        .launch()
        .await
}

fn start_db_connection(lola_server_config: &LolaServerConfig) -> Result<(), String> {
    db::set_database_url(&lola_server_config.database_url);
    db::start_connection()
        .map_err(|api_err| api_err.into_internal_string())?;
    Ok(())
}

async fn real_main() -> Result<(), Box<dyn Error>> {
    let cli_config = CliConfig::new();
    let lola_server_config = lola_server_config()?;

    match cli_config.command {
        Some(CliCommand::HashPassword) => {
            command::hash_password::run(lola_server_config.hash_salt.as_deref());
        }
        Some(CliCommand::GenSalt) => {
            command::gen_salt::run();
        }
        Some(CliCommand::ShowStats) => {
            start_db_connection(&lola_server_config)?;
            command::show_stats::run()
                .map_err(|api_err| api_err.into_internal_string())?;
        }
        None => {
            start_db_connection(&lola_server_config)?;
            start_server(lola_server_config).await?;
        }
    }

    Ok(())
}

#[rocket::main]
async fn main() {
    if let Err(err) = real_main().await {
        eprintln!("{}", err);
        std::process::exit(1);
    }
}
