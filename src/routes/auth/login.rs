use argon2::password_hash::Error;
use rocket::State;
use rocket::form::Form;
use rocket::serde::Serialize;

use crate::app_config::LolaServerConfig;
use crate::api::ApiResponse;
use crate::security::hash::verify_password;
use crate::security::jwt;

#[derive(FromForm)]
pub struct Body {
    username: String,
    password: String,
}

#[derive(Serialize)]
pub struct Payload {
    token: String,
}

#[post("/admin/login", data="<body>")]
pub fn route(
    body: Form<Body>,
    config: &State<LolaServerConfig>,
) -> ApiResponse<Payload> {
    macro_rules! return_error {
        ($msg:expr) => { return ApiResponse::error($msg) }
    }

    let admin = match &config.admin {
        Some(admin) => admin,
        None => return_error!("Admin connection is not available"),
    };

    let password_is_verified = match verify_password(body.password.as_bytes(), &admin.password) {
        Ok(()) => true,
        Err(Error::Password) => false,
        Err(err) => {
            eprintln!("Warning: error occured while trying to verify password: {}", err);
            return_error!("Admin connection is not available");
        }
    };

    if body.username == admin.username && password_is_verified {
        let token = jwt::create(&body.username);
        ApiResponse::success(Payload { token })
    } else {
        return_error!("Bad credentials");
    }
}
