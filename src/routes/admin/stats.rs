use std::sync::Mutex;

use rocket::State;

use crate::{
    api::ApiResponse,
    app::App,
    security::AdminUser,
    session::{SessionManagerMessage, SessionManagerStats},
};

#[get("/admin/stats")]
pub fn route(
    _admin_user: AdminUser,
    app: &State<Mutex<App>>,
) -> ApiResponse<SessionManagerStats> {
    let stats = send_message!(app, tx,
        SessionManagerMessage::GetGlobalStats(tx)
    );

    ApiResponse::from(stats)
}
