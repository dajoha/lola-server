use std::sync::Mutex;

use rocket::State;
use rocket::form::Form;

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(FromForm)]
pub struct Body {
    session_id: SessionId,
}

#[put("/sessions/<session_id>/rename", data="<body>")]
pub fn route(
    session_id: SessionId,
    body: Form<Body>,
    app: &State<Mutex<App>>
) -> ApiResponse {
    let new_session_id = body.session_id.clone();
    let result = send_message!(app, tx,
        SessionManagerMessage::RenameSession(session_id, new_session_id, tx)
    );

    match result {
        Ok(()) => ApiResponse::success_no_payload(),
        Err(err) => err.into(),
    }
}
