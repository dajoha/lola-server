use std::sync::Mutex;

use rocket::State;
use rocket::serde::Serialize;

use crate::App;
use crate::api::ApiResponse;
use crate::serialize::SessionInformation;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(Serialize)]
pub struct Payload {
    session: SessionInformation,
}

#[get("/sessions/<session_id>")]
pub fn route(session_id: SessionId, app: &State<Mutex<App>>) -> ApiResponse<Payload> {
    let result = send_message!(app, tx,
        SessionManagerMessage::GetSessionInformation(session_id, tx)
    );

    match result {
        Ok(session) => ApiResponse::success(Payload { session }),
        Err(err) => err.into(),
    }
}
