use std::sync::Mutex;

use rocket::State;
use rocket::serde::Serialize;
use diesel::prelude::*;

use crate::App;
use crate::api::ApiResponse;
use crate::error::ApiResult;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;
use crate::serialize::SessionSummary;
use crate::has_database;
use crate::db::models::FetchSession;

#[derive(Serialize)]
pub struct Payload {
    sessions: Vec<SessionSummary>,
}

fn get_sessions(app: &State<Mutex<App>>) -> ApiResult<Vec<SessionSummary>> {
    let sessions = if has_database!() {
        use crate::db::schema::sessions::dsl::*;

        lock_db_connection!(connection, {
            sessions
                .filter(is_public.eq(true))
                .load::<FetchSession>(connection)
                .ok().unwrap().iter()
                .map(|fetch_session| SessionSummary {
                    session_id: SessionId::new_unchecked(fetch_session.id.to_string())
                })
                .collect::<Vec<_>>()
        })
    } else {
        send_message!(app, tx,
            SessionManagerMessage::ListPublicSessionsSummaries(tx)
        )
    };

    Ok(sessions)
}

#[get("/list_sessions")]
pub fn route(app: &State<Mutex<App>>) -> ApiResponse<Payload> {
    get_sessions(app)
        .map(|sessions| Payload { sessions })
        .into()
}
