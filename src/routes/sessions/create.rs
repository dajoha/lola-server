use std::sync::Mutex;

use rocket::serde::Serialize;
use rocket::State;
use rocket::form::Form;

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(FromForm)]
pub struct Body {
    session_id: Option<SessionId>,
}

#[derive(Serialize)]
pub struct Payload {
    session_id: SessionId,
}

#[post("/sessions", data="<body>")]
pub fn route(body: Form<Body>, app: &State<Mutex<App>>) -> ApiResponse<Payload> {
    let result = match body.session_id.clone() {
        Some(session_id) => {
            send_message!(app, tx,
                SessionManagerMessage::CreateNamedSession(session_id.clone(), tx)
            ).map(|_| session_id)
        },
        None => {
            send_message!(app, tx,
                SessionManagerMessage::CreateSession(tx)
            )
        },
    };

    match result {
        Ok(session_id) => ApiResponse::success(Payload { session_id }),
        Err(err) => err.into(),
    }
}
