use clap::{Arg, App};

/// Stores the configuration retrieved from the command-line arguments.
#[derive(Debug)]
pub struct CliConfig {
    pub command: Option<CliCommand>,
}

#[derive(Debug)]
pub enum CliCommand {
    HashPassword,
    GenSalt,
    ShowStats,
}

impl CliConfig {
    pub fn new() -> CliConfig {
        let matches = Self::cli_definition().get_matches();

        let command = if matches.is_present("hash-password") {
            Some(CliCommand::HashPassword)
        } else if matches.is_present("gen-salt") {
            Some(CliCommand::GenSalt)
        } else if matches.is_present("stats") {
            Some(CliCommand::ShowStats)
        } else {
            None
        };

        Self { command }
    }

    /// Returns the command-line argument definition for clap.
    fn cli_definition<'a, 'b>() -> App<'a, 'b> {
        App::new("lola-server")
            .about("API server for the Lola language.")
            .arg(Arg::with_name("hash-password")
                .short("H")
                .long("hash-password")
                .conflicts_with_all(&["gen-salt", "stats"])
                .help("Hash a password interactively"))
            .arg(Arg::with_name("gen-salt")
                .short("S")
                .long("gen-salt")
                .conflicts_with("stats")
                .help("Generates a new salt for password hashes"))
            .arg(Arg::with_name("stats")
                .short("s")
                .long("stat")
                .help("Show stats information and quit"))
    }
}
