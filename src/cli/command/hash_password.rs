use std::io::prelude::*;
use std::io;

use termios::{Termios, tcsetattr};

use crate::security::hash::hash_password;

pub fn run(salt: Option<&str>) {
    print!("Password to hash: ");
    flush();

    let echo_off = EchoOff::new();

    let mut password = String::new();
    let readline_res = io::stdin().read_line(&mut password);
    if password.ends_with('\n') {
        password.remove(password.len() - 1);
    }
    println!();

    drop(echo_off);

    match readline_res {
        Ok(_) => (),
        Err(err) => {
            eprintln!("error: {}", err);
            std::process::exit(1);
        }
    }

    match hash_password(salt, password.as_bytes()) {
        Ok(hashed) => println!("{}", hashed),
        Err(err) => {
            eprintln!("error: {}", err);
            std::process::exit(1);
        }
    }
}

fn flush() {
    let _ = io::stdout().flush().ok();
}

struct EchoOff {
    term: Option<Termios>,
}

impl EchoOff {
    pub fn new() -> Self {
        let term = if let Ok(mut termios) = Termios::from_fd(0) {
            let c_lflag = termios.c_lflag;
            termios.c_lflag &= !termios::ECHO;

            if let Ok(()) = tcsetattr(0, termios::TCSADRAIN, &termios) {
                termios.c_lflag = c_lflag;
            }
            Some(termios)
        } else {
            None
        };
        Self { term }
    }
}

impl Drop for EchoOff {
    fn drop(&mut self) {
        if let Some(term) = self.term {
            tcsetattr(0, termios::TCSADRAIN, &term).unwrap_or(());
        }
    }
}
