use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::mysql::MysqlConnection;

use crate::db::schema::{sessions, commands};
use crate::session::{Session, SessionCommand, SessionId};
use crate::has_database;

#[derive(Queryable)]
pub struct FetchSession {
    pub db_id: i32,
    pub id: String,
    pub is_public: bool,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

impl FetchSession {
    /// Tries to fetch the given session from the database.
    pub fn find_by_id(session_id: &SessionId, connection: &MysqlConnection) -> Option<FetchSession> {
        if has_database!() {
            use crate::db::schema::sessions::dsl::*;

            sessions
                .filter(id.eq(session_id.inner_str()))
                .first::<FetchSession>(connection)
                .ok()
        } else {
            None
        }
    }
}

#[derive(Insertable)]
#[table_name="sessions"]
pub struct NewSession<'a> {
    pub id: &'a str,
    pub is_public: bool,
}

impl<'a> From<&'a Session> for NewSession<'a> {
    fn from(session: &'a Session) -> NewSession<'a> {
        NewSession {
            id: session.id().inner_str(),
            is_public: session.is_public(),
        }
    }
}

#[derive(Queryable)]
pub struct FetchCommand {
    pub db_id: i32,
    pub id: u64,
    pub input: String,
    pub session_db_id: i32,
    pub position: i32,
}

#[derive(Debug, Insertable)]
#[table_name="commands"]
pub struct NewCommand<'a> {
    pub id: u64,
    pub input: &'a str,
    pub session_db_id: i32,
    pub position: i32,
}

impl<'a> NewCommand<'a> {
    pub fn from_command(command: &'a SessionCommand, session_db_id: i32, position: i32) -> NewCommand {
        NewCommand {
            id: command.command_id(),
            input: command.input(),
            session_db_id,
            position,
        }
    }
}
