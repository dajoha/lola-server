pub mod one_or_many;
pub mod insert_at;

use std::borrow::Cow;

pub use one_or_many::*;
pub use insert_at::*;

pub type CowStr = Cow<'static, str>;

macro_rules! send_err_to_tx {
    ($tx:expr, $block:block) => {{
        let tx = $tx.clone();

        #[allow(clippy::redundant_closure_call)]
        let exec = || -> ApiResult {
            $block;
            Ok(())
        };

        if let Err(err) = exec() {
            tx.send(Err(err)).unwrap();
        }
    }}
}
