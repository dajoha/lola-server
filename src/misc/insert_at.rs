use crate::misc::{OneOrMany, OneOrManyIter};

pub struct InsertAtStruct<I>
where
    I: Iterator
{
    index: usize,
    inner_iter: I,
    insert_position: usize,
    elements: OneOrManyIter<I::Item>,
}

impl<I> Iterator for InsertAtStruct<I>
where
    I: Iterator
{
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index == self.insert_position {
            if let Some(element) = self.elements.next() {
                return Some(element);
            }
        }

        self.index += 1;
        self.inner_iter.next()
    }
}

pub trait InsertAt: Iterator
{
    fn insert_at(self, insert_position: usize, elements: OneOrMany<Self::Item>) -> InsertAtStruct<Self>
    where
        Self: Sized
    {
        InsertAtStruct {
            index: 0,
            inner_iter: self,
            insert_position,
            elements: elements.into_iter(),
        }
    }
}

impl<T> InsertAt for T where T: Iterator {}
