CREATE TABLE commands (
    db_id INT AUTO_INCREMENT PRIMARY KEY,
    id BIGINT UNSIGNED NOT NULL UNIQUE,
    input MEDIUMTEXT NOT NULL,
    session_db_id INT NOT NULL,
    position INT NOT NULL,
    CONSTRAINT fk_command_session
        FOREIGN KEY (session_db_id) REFERENCES sessions (db_id)
        ON DELETE CASCADE
);

# vim: ft=mysql
