CREATE TABLE sessions (
    db_id INT AUTO_INCREMENT PRIMARY KEY,
    id VARCHAR(255) UNIQUE NOT NULL,
    is_public BOOL NOT NULL DEFAULT 0
);

# vim: ft=mysql
